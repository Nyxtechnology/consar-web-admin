import React from 'react'
import actions from '../../actions/'
import style from './style.css'
import Layout from '../../components/Layout/'
import Dashboard from '../Dashboard/'

class Home extends React.Component {
  render () {
    return (
      <Layout {...this.props}>
        <div className={style.home}>
          <img src='/static/img/logo.jpg' />
        </div>
      </Layout>
    )
  }
}

export default actions(props => props.auth.process
    ? null : props.auth.user.token
    ? <Dashboard {...props} /> : <Home {...props} />)
