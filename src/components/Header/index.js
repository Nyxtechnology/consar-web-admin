import React from 'react'
import {Link} from 'react-router-dom'
import style from './style.css'

const UserButton = props => (
  <div className={style.userButton}>
    <div onClick={() => props.setModal('usermenu')}>
      <p>{props.auth.user.username}</p>
    </div>
    {props.general.modal === 'usermenu'
      ? <div className={style.userMenu}>
        <a onClick={props.logout}>Logout</a>
      </div>
      : null
    }
  </div>
)
const Menu = props => (
  <div className={style.menu}>
    <Link to='/login'>Login</Link>
  </div>
)

const Navbar = props => (
  <header className={style.header}>
    <nav className={style.nav}>
      <div>
        <div>
          <a href='/'>
            CONSAR
          </a>
        </div>
        {props.auth.user.token ? <UserButton {...props} /> : <Menu />}
      </div>
    </nav>
  </header>
)

export default Navbar
