import findIndex from 'lodash/findIndex'
const initialState = {
  process: null,
  err: null,
  step: 0,
  list: [],
  data: {
    name: '',
    title: '',
    description: '',
    text: '',
    icon: '',
    image: '',
    video: ''
  }
}

export default function projects (state = initialState, action) {
  switch (action.type) {
    case 'projectsChange':
      return {...state, err: null, data: {...state.data, ...action.data}}
    case 'projectsSelect':
      return {...state, err: null, data: state.data.id === action.data.id ? initialState.data : action.data}
    case 'projectsInitReset':
      return initialState
    case 'projectsInitAttempt':
      return {...state, process: true, err: null}
    case 'projectsInitSuccess':
      return {...state, process: false, ...action.data}
    case 'projectsReset':
      return {...state, data: initialState.data}
    case 'projectsLoadAttempt':
      return {...state, process: 'load', err: null}
    case 'projectsLoadSuccess':
      return {...state, process: false, err: null, list: action.data}
    case 'projectsLoadError':
      return {...state, process: null, err: action.data}
    case 'projectsLoadOneAttempt':
      return {...state, process: 'load', err: null}
    case 'projectsLoadOneSuccess':
      return {...state, process: false, err: null, data: {...initialState.data, ...action.data}}
    case 'projectsLoadOneError':
      return {...state, process: null, err: action.data}
    case 'projectsSaveAttempt':
      return {...state, process: 'create', err: null}
    case 'projectsSaveSuccess':
      return {
        ...initialState,
        data: action.data,
        list: findIndex(state.list, {id: action.data.id}) === -1
          ? [...state.list, action.data]
          : state.list.map(r => r.id === action.data.id ? action.data : r)
      }
    case 'projectsSaveError':
      return {...state, process: null, err: action.data}
    case 'projectsDeleteAttempt':
      return {...state, process: 'delete', err: null}
    case 'projectsDeleteSuccess':
      return {
        ...initialState,
        list: state.list.filter(i => i.id !== action.data)
      }
    case 'projectsDeleteError':
      return {...state, process: null, err: action.data}
    default:
      return state
  }
}
