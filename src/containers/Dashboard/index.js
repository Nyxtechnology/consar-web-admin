import React from 'react'
import Layout from '../../components/Layout/'
import actions from '../../actions/'
import style from './style.css'
import {Link} from 'react-router-dom'

class Dashboard extends React.Component {
  componentDidMount () {
    if (!this.props.auth.user.token) return this.props.history.push('/')
  }
  render () {
    return (
      <Layout {...this.props}>
        <div className={style.dashboardContainer}>
          <div>
            <Link to='/usuarios'>Usuarios</Link>
            <Link to='/proyectos'>Proyectos</Link>
            <Link to='/categorias'>Categorias</Link>
          </div>
        </div>
      </Layout>
    )
  }
}

export default actions(Dashboard)
