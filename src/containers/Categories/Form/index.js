import React from 'react'
import {Input} from 'react-one-form'
import {Link} from 'react-router-dom'
import style from './style.css'
import Links from '../../Links/'

export default class CategoriesForm extends React.Component {
  componentDidMount () {
    if (this.props.match.params.slug) {
      this.props.categoriesLoadOne(this.props.match.params.slug)
    } else {
      this.props.categoriesReset()
    }
  }
  render () {
    const config = {store: this.props.categories, change: this.props.categoriesInput, style}
    const process = this.props.categories.process
    return (
      <div className={style.formContainer}>
        <div className={style.formHeader}>
          <h3>{this.props.categories.data.id ? 'Editar Categoría' : 'Crear Categoría'}</h3>
          <Link className={style.backBtn} to='/categorias'>Volver</Link>
        </div>
        <form className={style.courseForm}>
          <Input {...{...config, id: 'type', label: 'Tipo', type: 'select'}} />
          <Input {...{...config, id: 'title', label: 'Titulo'}} />
          <Input {...{...config, id: 'img0', label: 'Imagen Jovenes', type: 'file'}} />
          <Input {...{...config, id: 'img1', label: 'Imagen Adultos', type: 'file'}} />
          <Input {...{...config, id: 'img2', label: 'Imagen Mayores', type: 'file'}} />
        </form>
        <div className={style.btns}>
          <button className={style.publishBtn} onClick={this.props.categoriesSave}>
            {process === 'save' ? <div><i className='fa fa-spinner fa-spin' /></div> : 'Guardar'}
          </button>
          {this.props.categories.data.id &&
            <button className={style.publishBtn} onClick={e => this.props.setModal('categoryDelete')}>
              {process === 'delete' ? <div><i className='fa fa-spinner fa-spin' /></div> : 'Eliminar'}
            </button>
          }
        </div>

        <Links {...this.props} />
      </div>
    )
  }
}
