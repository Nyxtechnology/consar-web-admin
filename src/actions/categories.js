import {getCategories, saveCategory, deleteCategories} from '../helpers/services'
import {closeAll} from './general'
import history from '../helpers/history'

export const categoriesReset = data => ({type: 'categoriesReset', data})
export const categoriesChange = data => ({type: 'categoriesChange', data})
export const categoriesSelect = data => ({type: 'categoriesSelect', data})
export const categoriesInput = e => {
  e.preventDefault()
  const type = e.target.type
  const name = type === 'radio' ? e.target.name : e.target.id
  const value =
    type === 'checkbox' ? e.target.checked
    : type === 'file' ? e.target.files[0]
    : e.target.value
  return categoriesChange({[name]: value})
}

export const categoriesLoadAttempt = () => ({type: 'categoriesLoadAttempt'})
export const categoriesLoadSuccess = data => ({type: 'categoriesLoadSuccess', data})
export const categoriesLoadError = data => ({type: 'categoriesLoadError', data})
export const categoriesLoad = (params) => (dispatch, getState) => {
  const {process} = getState().categories
  if (process) return
  dispatch(categoriesLoadAttempt())
  return getCategories(getState().auth.user.token, params)
  .then(({data}) => dispatch(categoriesLoadSuccess(data)))
  .catch(res => dispatch(categoriesLoadError(res.data)))
}

export const categoriesLoadOneAttempt = () => ({type: 'categoriesLoadOneAttempt'})
export const categoriesLoadOneSuccess = data => ({type: 'categoriesLoadOneSuccess', data})
export const categoriesLoadOneError = data => ({type: 'categoriesLoadOneError', data})
export const categoriesLoadOne = (slug) => (dispatch, getState) => {
  const {process} = getState().categories
  if (process) return
  dispatch(categoriesLoadOneAttempt())
  return getCategories(getState().auth.user.token, slug)
  .then(({data}) => dispatch(categoriesLoadOneSuccess(data)))
  .catch(res => dispatch(categoriesLoadOneError(res.data)))
}

export const categoriesSaveAttempt = () => ({type: 'categoriesSaveAttempt'})
export const categoriesSaveSuccess = data => ({type: 'categoriesSaveSuccess', data})
export const categoriesSaveError = data => ({type: 'categoriesSaveError', data})
export const categoriesSave = () => (dispatch, getState) => {
  const {process, data} = getState().categories
  if (process) return
  dispatch(categoriesSaveAttempt())
  return saveCategory(getState().auth.user.token, data)
  .then(({data}) => {
    dispatch(categoriesSaveSuccess(data))
    history.push('/categorias/' + data.slug)
  })
  .catch(res => {
    console.log(res)
    dispatch(categoriesSaveError(res.data))
  })
}

export const categoriesDeleteAttempt = () => ({type: 'categoriesDeleteAttempt'})
export const categoriesDeleteSuccess = data => ({type: 'categoriesDeleteSuccess', data})
export const categoriesDeleteError = data => ({type: 'categoriesDeleteError', data})
export const categoriesDelete = () => (dispatch, getState) => {
  const {process, data} = getState().categories
  if (process) return
  dispatch(categoriesDeleteAttempt())
  return deleteCategories(getState().auth.user.token, data.id)
  .then(res => {
    dispatch(categoriesDeleteSuccess(data.id))
    dispatch(closeAll())
    history.push('/categorias')
  })
  .catch(res => {
    console.log(res)
    dispatch(categoriesDeleteError(res.data))
  })
}
