import {getLinks, saveLinks, deleteLinks, reorderLinks} from '../helpers/services'
import {closeAll} from './general'
import {categoriesChange} from './categories'
import sortBy from 'lodash/sortBy'

export const linksReset = data => ({type: 'linksReset', data})
export const linksChange = data => ({type: 'linksChange', data})
export const linksSelect = data => ({type: 'linksSelect', data})
export const linksInput = e => {
  e.preventDefault()
  const type = e.target.type
  const name = type === 'radio' ? e.target.name : e.target.id
  const value =
    type === 'checkbox' ? e.target.checked
    : type === 'file' ? e.target.files[0]
    : e.target.value
  return linksChange({[name]: value})
}

export const linksLoadAttempt = () => ({type: 'linksLoadAttempt'})
export const linksLoadSuccess = data => ({type: 'linksLoadSuccess', data})
export const linksLoadError = data => ({type: 'linksLoadError', data})
export const linksLoad = () => (dispatch, getState) => {
  const {process} = getState().links
  const token = getState().auth.user.token
  const referenceId = getState().categories.data.id
  if (process) return
  dispatch(linksLoadAttempt())
  getLinks(token, null, {referenceId})
  .then(res => dispatch(linksLoadSuccess(sortBy(res.data, ['order', 'id']))))
  .catch(res => dispatch(linksLoadError(res.data)))
}

export const linksLoadOneAttempt = () => ({type: 'linksLoadOneAttempt'})
export const linksLoadOneSuccess = data => ({type: 'linksLoadOneSuccess', data})
export const linksLoadOneError = data => ({type: 'linksLoadOneError', data})
export const linksLoadOne = (slug, params) => (dispatch, getState) => {
  const {process} = getState().links
  const token = getState().auth.user.token
  if (process) return
  dispatch(linksLoadOneAttempt())
  return getLinks(token, slug, params)
  .then(res => dispatch(linksLoadOneSuccess(res.data)))
  .catch(res => dispatch(linksLoadOneError(res.data)))
}

export const linksSaveAttempt = () => ({type: 'linksSaveAttempt'})
export const linksSaveSuccess = data => ({type: 'linksSaveSuccess', data})
export const linksSaveError = data => ({type: 'linksSaveError', data})
export const linksSave = () => (dispatch, getState) => {
  const {process, data} = getState().links
  const referenceId = getState().categories.data.id
  if (process) return
  dispatch(linksSaveAttempt())
  return saveLinks(getState().auth.user.token, {...data, referenceId})
  .then(({data}) => {
    dispatch(closeAll())
    dispatch(linksSaveSuccess(data))
  })
  .catch(res => {
    console.log(res)
    dispatch(linksSaveError(res.data))
  })
}

export const linksDeleteAttempt = () => ({type: 'linksDeleteAttempt'})
export const linksDeleteSuccess = data => ({type: 'linksDeleteSuccess', data})
export const linksDeleteError = data => ({type: 'linksDeleteError', data})
export const linksDelete = () => (dispatch, getState) => {
  const {process, data} = getState().links
  if (process) return
  dispatch(linksDeleteAttempt())
  return deleteLinks(getState().auth.user.token, data.id)
  .then(res => {
    dispatch(linksDeleteSuccess(data.id))
    dispatch(closeAll())
  })
  .catch(res => {
    console.log(res)
    dispatch(linksDeleteError(res.data))
  })
}

export const LinksReorder = (data) => (dispatch, getState) => {
  const {process, list} = getState().links
  if (process) return

  const draggingOverIndex = list.findIndex(item => item.id === data.draggingOver.id)
  const filtered = list.filter(item => item.id !== data.dragging.id)
  const reordered = [...filtered.slice(0, draggingOverIndex), data.dragging, ...filtered.slice(draggingOverIndex)]
  const reorderedIds = reordered.map(i => i.id)
  dispatch(categoriesChange({}))
  reorderLinks(getState().auth.user.token, data)
  dispatch(linksLoadSuccess(reordered))
}
