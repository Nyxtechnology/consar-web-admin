require('dotenv').config()
const path = require('path')
const webpack = require('webpack')

module.exports = {
  entry: [
    'react-hot-loader/patch',
    'webpack-hot-middleware/client',
    path.join(__dirname, './src/app.js')
  ],
  output: {
    path: path.resolve('static'),
    filename: 'main.js',
    publicPath: '/static/development/'
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.DefinePlugin({
      'process.env': {
        'target': JSON.stringify(process.env.TARGET),
        'NODE_ENV': JSON.stringify(process.env.NODE_ENV),
        'targetenv': JSON.stringify('browser'),
        'api': JSON.stringify(process.env.APIURL)
      }
    })
  ],
  module: {
    rules: [
      { test: /\.js$/, use: ['babel-loader'], include: path.join(__dirname, 'src') },
      { test: /\.(css)$/,
        use: [
          'style-loader',
          {
            loader: 'css-loader',
            options: { modules: true, importLoaders: 1, localIdentName: '[name]__[local]___[hash:base64:5]' }
          },
          'postcss-loader'
        ]
      }
    ]
  }
}
