[![JavaScript Style Guide](https://cdn.rawgit.com/feross/standard/master/badge.svg)](https://github.com/feross/standard)

Cargopi web client
===============

## Installation

### Clone
```bash
git clone git+ssh://git@gitlab.com/calderaro941/cargopiweb.git
```
### Install dependencies
```bash
npm install
```
### Run production server
```bash
 npm run production
```
### Run development server
```bash
 npm run start-dev  
```
### Node Version

> Node v8.1.2
