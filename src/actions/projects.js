import {getProjects, saveProjects, deleteProjects} from '../helpers/services'
import {closeAll} from './general'
import history from '../helpers/history'

export const projectsReset = data => ({type: 'projectsReset', data})
export const projectsChange = data => ({type: 'projectsChange', data})
export const projectsSelect = data => ({type: 'projectsSelect', data})
export const projectsInput = e => {
  e.preventDefault()
  const type = e.target.type
  const name = type === 'radio' ? e.target.name : e.target.id
  const value =
    type === 'checkbox' ? e.target.checked
    : type === 'file' ? e.target.files[0]
    : e.target.value
  return projectsChange({[name]: value})
}

export const projectsLoadAttempt = () => ({type: 'projectsLoadAttempt'})
export const projectsLoadSuccess = data => ({type: 'projectsLoadSuccess', data})
export const projectsLoadError = data => ({type: 'projectsLoadError', data})
export const projectsLoad = (params) => (dispatch, getState) => {
  const {process} = getState().projects
  if (process) return
  dispatch(projectsLoadAttempt())
  return getProjects(getState().auth.user.token, params)
  .then(({data}) => dispatch(projectsLoadSuccess(data)))
  .catch(res => dispatch(projectsLoadError(res.data)))
}

export const projectsLoadOneAttempt = () => ({type: 'projectsLoadOneAttempt'})
export const projectsLoadOneSuccess = data => ({type: 'projectsLoadOneSuccess', data})
export const projectsLoadOneError = data => ({type: 'projectsLoadOneError', data})
export const projectsLoadOne = (slug) => (dispatch, getState) => {
  const {process} = getState().projects
  if (process) return
  dispatch(projectsLoadOneAttempt())
  return getProjects(getState().auth.user.token, slug)
  .then(({data}) => dispatch(projectsLoadOneSuccess(data)))
  .catch(res => dispatch(projectsLoadOneError(res.data)))
}

export const projectsSaveAttempt = () => ({type: 'projectsSaveAttempt'})
export const projectsSaveSuccess = data => ({type: 'projectsSaveSuccess', data})
export const projectsSaveError = data => ({type: 'projectsSaveError', data})
export const projectsSave = () => (dispatch, getState) => {
  const {process, data} = getState().projects
  if (process) return
  dispatch(projectsSaveAttempt())
  return saveProjects(getState().auth.user.token, data)
  .then(({data}) => {
    dispatch(projectsSaveSuccess(data))
    history.push('/proyectos/' + data.slug)
  })
  .catch(res => {
    console.log(res)
    dispatch(projectsSaveError(res.data))
  })
}

export const projectsDeleteAttempt = () => ({type: 'projectsDeleteAttempt'})
export const projectsDeleteSuccess = data => ({type: 'projectsDeleteSuccess', data})
export const projectsDeleteError = data => ({type: 'projectsDeleteError', data})
export const projectsDelete = () => (dispatch, getState) => {
  const {process, data} = getState().projects
  if (process) return
  dispatch(projectsDeleteAttempt())
  return deleteProjects(getState().auth.user.token, data.id)
  .then(res => {
    dispatch(projectsDeleteSuccess(data.id))
    dispatch(closeAll())
    history.push('/proyectos')
  })
  .catch(res => {
    console.log(res)
    dispatch(projectsDeleteError(res.data))
  })
}
