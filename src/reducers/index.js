import {combineReducers} from 'redux'
import general from './general'
import auth from './auth'
import categories from './categories'
import links from './links'
import projects from './projects'
import users from './users'

const rootReducer = combineReducers({
  general,
  auth,
  categories,
  links,
  projects,
  users
})

export default rootReducer
