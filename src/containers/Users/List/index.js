import React from 'react'
import style from './style.css'
import {Link} from 'react-router-dom'

class UsersList extends React.Component {
  componentDidMount () {
    if (!this.props.auth.user.token) return this.props.history.push('/')
    this.props.usersLoad()
  }
  render () {
    const process = this.props.users.process
    return (
      <div className={style.listContainer}>
        <div className={style.listHeader}>
          <h3>Usuarios</h3>
          <Link to='/usuarios/nuevo'>Crear Usuario</Link>
        </div>
        <div className={style.list}>
          {this.props.users.list.map(i => (
            <Link
              key={i.id}
              to={'/usuarios/' + i.id}>
              <div>{i.username}</div>
            </Link>
          ))}
        </div>
      </div>
    )
  }
}

export default UsersList
