import React from 'react'
import get from 'lodash/fp/get'
import {Input} from 'react-one-form'
import Error from '../Error/'
import style from './style.css'

const Register = props => {
  const {process, err} = props.auth
  const config = {store: props.auth, change: props.authInput, style}
  const state = get('location.state', props)
  return (
    <div className={style.login}>
      <h1>Registro</h1>
      <form onKeyDown={e => e.key === 'Enter' ? props.register(state) : null}>
        <div className={style.inputsWrapper}>
          <Input {...{...config, id: 'username', label: 'Usuario'}} />
          <Input {...{...config, id: 'password', type: 'password', label: 'Contraseña'}} />
          <Input {...{...config, id: 'confirmPassword', type: 'password', label: 'Repite Contraseña'}} />
        </div>
        <Error err={err} />
        <button type='button' onClick={() => props.register(state)}>
          {process ? <i className='fa fa-circle-o-notch fa-spin' /> : 'Registrar'}
        </button>
      </form>
    </div>
  )
}

export default Register
