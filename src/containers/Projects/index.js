import React from 'react'
import {Route, Switch} from 'react-router-dom'
import actions from '../../actions/'
import Layout from '../../components/Layout/'
import Form from './Form/'
import List from './List/'
import Delete from './Delete/'

class Projects extends React.Component {
  componentDidMount () {
    if (!this.props.auth.user.token) return this.props.history.push('/')
  }
  render () {
    return (
      <Layout {...this.props}>
        {this.props.general.modal === 'projectDelete' ? <Delete {...this.props} /> : null}
        <Switch>
          <Route path='/proyectos' exact render={props => <List {...{...this.props, ...props}} />} />
          <Route path='/proyectos/nuevo' exact render={props => <Form {...{...this.props, ...props}} />} />
          <Route path='/proyectos/:slug' exact render={props => <Form {...{...this.props, ...props}} />} />
        </Switch>
      </Layout>
    )
  }
}

export default actions(props => props.auth.process ? null : <Projects {...props} />)
