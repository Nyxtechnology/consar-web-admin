import findIndex from 'lodash/findIndex'
const initialState = {
  process: null,
  err: null,
  step: 0,
  referenceIdList: [],
  list: [],
  data: {
    referenceId: '',
    title: '',
    img: '',
    text: '',
    href: ''
  }
}

export default function links (state = initialState, action) {
  switch (action.type) {
    case 'linksChange':
      return {...state, err: null, data: {...state.data, ...action.data}}
    case 'linksSelect':
      return {...state, err: null, data: {...initialState.data, ...action.data}}
    case 'linksInitReset':
      return initialState
    case 'linksInitAttempt':
      return {...state, process: true, err: null}
    case 'linksInitSuccess':
      return {...state, process: false, ...action.data}
    case 'linksReset':
      return {...state, data: initialState.data}
    case 'linksLoadAttempt':
      return {...state, process: 'load', err: null}
    case 'linksLoadSuccess':
      return {
        ...state,
        process: false,
        err: null,
        list: action.data
      }
    case 'linksLoadError':
      return {...state, process: null, err: action.data}
    case 'linksLoadOneAttempt':
      return {...state, process: 'load', err: null}
    case 'linksLoadOneSuccess':
      return {
        ...initialState,
        list: state.list,
        data: {...initialState.data, ...action.data}
      }
    case 'linksLoadOneError':
      return {...state, process: null, err: action.data}
    case 'linksSaveAttempt':
      return {...state, process: 'create', err: null}
    case 'linksSaveSuccess':
      return {
        ...initialState,
        list: findIndex(state.list, {id: action.data.id}) === -1
          ? [...state.list, action.data]
          : state.list.map(r => r.id === action.data.id ? action.data : r)
      }
    case 'linksSaveError':
      return {...state, process: null, err: action.data}
    case 'linksDeleteAttempt':
      return {...state, process: 'delete', err: null}
    case 'linksDeleteSuccess':
      return {
        ...initialState,
        list: state.list.filter(i => i.id !== action.data)
      }
    case 'linksDeleteError':
      return {...state, process: null, err: action.data}
    default:
      return state
  }
}
