import React from 'react'
import style from './style.css'

export default class LinksDelete extends React.Component {
  componentDidMount () {
    this.props.linksSelect(this.props.general.value || {})
  }
  render () {
    return (
      <div className={style.formContainer}>
        <div onClick={e => e.stopPropagation(e)}>
          <div className={style.formTitle}>Eliminar enlace</div>
          <p>¿Estas seguro que deseas eliminar el enlace {this.props.links.data.title}?</p>
          <div className={style.btns}>
            {this.props.links.data.id &&
              <button className={style.publishBtn} onClick={e => this.props.linksDelete()}>
                {this.props.links.process === 'delete' ? <div><i className='fa fa-spinner fa-spin' /></div> : 'Eliminar'}
              </button>
            }
            <a className={style.publishBtn} onClick={this.props.closeAll}>Cerrar</a>
          </div>
        </div>
      </div>
    )
  }
}
