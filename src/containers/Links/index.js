import React from 'react'
import Form from './Form/'
import List from './List/'
import Delete from './Delete/'

class Links extends React.Component {
  componentDidMount () {
    if (!this.props.auth.user.token) return this.props.history.push('/')
  }
  render () {
    return (
      <div>
        {this.props.general.modal === 'linkDelete' ? <Delete {...this.props} /> : null}
        {this.props.general.modal === 'linkForm' ? <Form {...this.props} /> : null}
        {this.props.categories.data.id ? <List {...this.props} /> : null}
      </div>
    )
  }
}

export default Links
