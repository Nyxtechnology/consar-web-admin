import {getUsers, saveUsers, deleteUsers} from '../helpers/services'
import {closeAll} from './general'
import history from '../helpers/history'

export const usersReset = data => ({type: 'usersReset', data})
export const usersChange = data => ({type: 'usersChange', data})
export const usersSelect = data => ({type: 'usersSelect', data})
export const usersInput = e => {
  e.preventDefault()
  const type = e.target.type
  const name = type === 'radio' ? e.target.name : e.target.id
  const value = e.target.value.replace(/\W/g, '')
  return usersChange({[name]: value})
}

export const usersLoadAttempt = () => ({type: 'usersLoadAttempt'})
export const usersLoadSuccess = data => ({type: 'usersLoadSuccess', data})
export const usersLoadError = data => ({type: 'usersLoadError', data})
export const usersLoad = (params) => (dispatch, getState) => {
  const {process} = getState().users
  if (process) return
  dispatch(usersLoadAttempt())
  return getUsers(getState().auth.user.token, params)
  .then(({data}) => dispatch(usersLoadSuccess(data)))
  .catch(res => dispatch(usersLoadError(res.data)))
}

export const usersLoadOneAttempt = () => ({type: 'usersLoadOneAttempt'})
export const usersLoadOneSuccess = data => ({type: 'usersLoadOneSuccess', data})
export const usersLoadOneError = data => ({type: 'usersLoadOneError', data})
export const usersLoadOne = (id) => (dispatch, getState) => {
  const {process} = getState().users
  if (process) return
  dispatch(usersLoadOneAttempt())
  return getUsers(getState().auth.user.token, id)
  .then(({data}) => dispatch(usersLoadOneSuccess(data)))
  .catch(res => dispatch(usersLoadOneError(res.data)))
}

export const usersSaveAttempt = () => ({type: 'usersSaveAttempt'})
export const usersSaveSuccess = data => ({type: 'usersSaveSuccess', data})
export const usersSaveError = data => ({type: 'usersSaveError', data})
export const usersSave = () => (dispatch, getState) => {
  const {process, data} = getState().users
  if (process) return
  dispatch(usersSaveAttempt())
  return saveUsers(getState().auth.user.token, data)
  .then(({data}) => {
    dispatch(usersSaveSuccess(data))
    history.push('/usuarios/' + data.id)
  })
  .catch(res => {
    console.log(res)
    dispatch(usersSaveError(res.data))
  })
}

export const usersDeleteAttempt = () => ({type: 'usersDeleteAttempt'})
export const usersDeleteSuccess = data => ({type: 'usersDeleteSuccess', data})
export const usersDeleteError = data => ({type: 'usersDeleteError', data})
export const usersDelete = () => (dispatch, getState) => {
  const {process, data} = getState().users
  if (process) return
  dispatch(usersDeleteAttempt())
  return deleteUsers(getState().auth.user.token, data.id)
  .then(res => {
    dispatch(usersDeleteSuccess(data.id))
    dispatch(closeAll())
    history.push('/usuarios')
  })
  .catch(res => {
    console.log(res)
    dispatch(usersDeleteError(res.data))
  })
}
