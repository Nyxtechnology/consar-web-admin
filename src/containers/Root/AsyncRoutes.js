import asyncComponent from '../../helpers/asyncComponent'

export const Login = asyncComponent('Login', () => import('../Login/'))
export const Register = asyncComponent('Register', () => import('../Register/'))
export const Home = asyncComponent('Home', () => import('../Home/'))
export const Categories = asyncComponent('Categories', () => import('../Categories/'))
export const Links = asyncComponent('Links', () => import('../Links/'))
export const Projects = asyncComponent('Projects', () => import('../Projects/'))
export const Users = asyncComponent('Users', () => import('../Users/'))
