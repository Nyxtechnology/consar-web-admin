import history from '../helpers/history'
import get from 'lodash/get'
import {slogin} from '../helpers/services'

export const authError = data => ({type: 'authError', data})
export const authInput = e => {
  const type = e.target.type
  const name = type === 'radio' ? e.target.name : e.target.id
  const value = type === 'checkbox' ? e.target.checked : e.target.value
  return authChange({[name]: value})
}

export const authAttempt = data => ({type: 'authAttempt', data})
export const authSuccess = data => ({type: 'authSuccess', data})
export const authChange = data => ({type: 'authChange', data})
export const login = (opt) => (dispatch, getState) => {
  const {process, data} = getState().auth
  if (process) return
  dispatch(authAttempt())
  slogin(data.username, data.password)
  .then(res => {
    dispatch(authSuccess(res.data))
    history.push(get(opt, 'back', '/'))
  })
  .catch((e) => {
    if (e.message === 'Network Error') return dispatch(authError({code: 'networkError'}))
    if (e.code) return dispatch(authError(e))
    dispatch(authError(e.data))
  })
}

export const relogin = ftoken => (dispatch, getState) => {
  const {process, data} = getState().auth
  if (process) return
  slogin(data.username, data.password)
  .then(res => dispatch(authSuccess({...res.data, ftoken})))
  .catch((e) => {
    if (e.message === 'Network Error') return dispatch(authError({code: 'networkError'}))
    if (e.code) return dispatch(authError(e))
    dispatch(authError(e.data))
  })
}

export const logout = () => (dispatch, getState) => {
  dispatch({type: 'authReset'})
  history.push('/')
}
