import React from 'react'
import {Route, Switch} from 'react-router-dom'
import actions from '../../actions/'
import Layout from '../../components/Layout/'
import Form from './Form/'
import List from './List/'
import Delete from './Delete/'

class Categories extends React.Component {
  componentDidMount () {
    if (!this.props.auth.user.token) return this.props.history.push('/')
  }
  render () {
    return (
      <Layout {...this.props}>
        {this.props.general.modal === 'categoryDelete' ? <Delete {...this.props} /> : null}
        <Switch>
          <Route path='/categorias' exact render={props => <List {...{...this.props, ...props}} />} />
          <Route path='/categorias/nuevo' exact render={props => <Form {...{...this.props, ...props}} />} />
          <Route path='/categorias/:slug' exact render={props => <Form {...{...this.props, ...props}} />} />
        </Switch>
      </Layout>
    )
  }
}

export default actions(props => props.auth.process ? null : <Categories {...props} />)
