import syncComponent from '../../helpers/syncComponent'

export const Login = syncComponent('Login', require('../Login'))
export const Register = syncComponent('Register', require('../Register'))
export const Home = syncComponent('Home', require('../Home'))
export const Categories = syncComponent('Categories', require('../Categories'))
export const Links = syncComponent('Links', require('../Links'))
export const Projects = syncComponent('Projects', require('../Projects'))
export const Users = syncComponent('Users', require('../Users'))
