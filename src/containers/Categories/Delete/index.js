import React from 'react'
import style from './style.css'

export default class LinksDelete extends React.Component {
  render () {
    return (
      <div className={style.formContainer}>
        <div onClick={e => e.stopPropagation(e)}>
          <div className={style.formTitle}>Eliminar Categoría</div>
          <p>¿Estas seguro que deseas eliminar la categoria <strong>{this.props.categories.data.title}</strong>?</p>
          <div className={style.btns}>
            {this.props.categories.data.id &&
              <button className={style.publishBtn} onClick={e => this.props.categoriesDelete()}>
                {this.props.categories.process === 'delete' ? <div><i className='fa fa-spinner fa-spin' /></div> : 'Eliminar'}
              </button>
            }
            <a className={style.publishBtn} onClick={this.props.closeAll}>Cerrar</a>
          </div>
        </div>
      </div>
    )
  }
}
