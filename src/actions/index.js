import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import * as general from './general'
import * as auth from './auth'
import * as categories from './categories'
import * as links from './links'
import * as projects from './projects'
import * as users from './users'

const actions = {
  ...general,
  ...auth,
  ...categories,
  ...links,
  ...projects,
  ...users
}

const con = Component => connect(state => state, dispatch => bindActionCreators(actions, dispatch))(Component)

export default con
