import React from 'react'
import style from './style.css'

export default class ProjectsDelete extends React.Component {
  render () {
    return (
      <div className={style.formContainer}>
        <div onClick={e => e.stopPropagation(e)}>
          <div className={style.formTitle}>Eliminar Usuario</div>
          <p>¿Estas seguro que deseas eliminar el usuario <strong>{this.props.users.data.username}</strong>?</p>
          <div className={style.btns}>
            {this.props.users.data.id &&
              <button className={style.publishBtn} onClick={e => this.props.usersDelete()}>
                {this.props.users.process === 'delete' ? <div><i className='fa fa-spinner fa-spin' /></div> : 'Eliminar'}
              </button>
            }
            <a className={style.publishBtn} onClick={this.props.closeAll}>Cerrar</a>
          </div>
        </div>
      </div>
    )
  }
}
