import React from 'react'
import {Input} from 'react-one-form'
import {Link} from 'react-router-dom'
import style from './style.css'

export default class UsersForm extends React.Component {
  componentDidMount () {
    if (this.props.match.params.id) {
      this.props.usersLoadOne(this.props.match.params.id)
    } else {
      this.props.usersReset()
    }
  }
  render () {
    const config = {store: this.props.users, change: this.props.usersInput, style}
    const process = this.props.users.process
    return (
      <div className={style.formContainer}>
        <div className={style.formHeader}>
          <h3>{this.props.users.data.id ? 'Editar Usuario' : 'Crear Usuario'}</h3>
          <Link className={style.backBtn} to='/usuarios'>Volver</Link>
        </div>
        <form className={style.courseForm}>
          <Input {...{...config, id: 'username', label: 'Nombre de usuario'}} />
          <Input {...{...config, id: 'password', label: 'Contraseña', type: 'password'}} />
        </form>
        <div className={style.btns}>
          <button className={style.publishBtn} onClick={this.props.usersSave}>
            {process === 'save' ? <div><i className='fa fa-spinner fa-spin' /></div> : 'Guardar'}
          </button>
          {this.props.users.data.id &&
            <button className={style.publishBtn} onClick={e => this.props.setModal('usersDelete')}>
              {process === 'delete' ? <div><i className='fa fa-spinner fa-spin' /></div> : 'Eliminar'}
            </button>
          }
        </div>
      </div>
    )
  }
}
