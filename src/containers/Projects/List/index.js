import React from 'react'
import style from './style.css'
import {Link} from 'react-router-dom'

class ProjectsList extends React.Component {
  componentDidMount () {
    if (!this.props.auth.user.token) return this.props.history.push('/')
    this.props.projectsLoad()
  }
  render () {
    const process = this.props.projects.process
    return (
      <div className={style.listContainer}>
        <div className={style.listHeader}>
          <h3>Proyectos</h3>
          <Link to='/proyectos/nuevo'>Crear Proyecto</Link>
        </div>
        <div className={style.list}>
          {this.props.projects.list.map(i => (
            <Link
              key={i.id}
              to={'/proyectos/' + i.slug}>
              <div>{i.title}</div>
            </Link>
          ))}
        </div>
      </div>
    )
  }
}

export default ProjectsList
