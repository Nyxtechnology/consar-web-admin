import findIndex from 'lodash/findIndex'
const initialState = {
  process: null,
  err: null,
  step: 0,
  list: [],
  data: {
    id: '',
    username: '',
    password: ''
  }
}

export default function users (state = initialState, action) {
  switch (action.type) {
    case 'usersChange':
      return {...state, err: null, data: {...state.data, ...action.data}}
    case 'usersSelect':
      return {...state, err: null, data: state.data.id === action.data.id ? initialState.data : action.data}
    case 'usersInitReset':
      return initialState
    case 'usersInitAttempt':
      return {...state, process: true, err: null}
    case 'usersInitSuccess':
      return {...state, process: false, ...action.data}
    case 'usersReset':
      return {...state, data: initialState.data}
    case 'usersLoadAttempt':
      return {...state, process: 'load', err: null}
    case 'usersLoadSuccess':
      return {...state, process: false, err: null, list: action.data}
    case 'usersLoadError':
      return {...state, process: null, err: action.data}
    case 'usersLoadOneAttempt':
      return {...state, process: 'load', err: null}
    case 'usersLoadOneSuccess':
      return {...state, process: false, err: null, data: {...initialState.data, ...action.data}}
    case 'usersLoadOneError':
      return {...state, process: null, err: action.data}
    case 'usersSaveAttempt':
      return {...state, process: 'create', err: null}
    case 'usersSaveSuccess':
      return {
        ...initialState,
        data: action.data,
        list: findIndex(state.list, {id: action.data.id}) === -1
          ? [...state.list, action.data]
          : state.list.map(r => r.id === action.data.id ? action.data : r)
      }
    case 'usersSaveError':
      return {...state, process: null, err: action.data}
    case 'usersDeleteAttempt':
      return {...state, process: 'delete', err: null}
    case 'usersDeleteSuccess':
      return {
        ...initialState,
        list: state.list.filter(i => i.id !== action.data)
      }
    case 'usersDeleteError':
      return {...state, process: null, err: action.data}
    default:
      return state
  }
}
