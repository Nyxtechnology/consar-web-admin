import React from 'react'
import {Input} from 'react-one-form'
import style from './style.css'

export default class LinksForm extends React.Component {
  componentDidMount () {
    this.props.linksSelect(this.props.general.value || {})
  }
  render = () => {
    const config = {store: this.props.links, change: this.props.linksInput, style}
    return (
      <div className={style.formContainer}>
        <div onClick={e => e.stopPropagation(e)}>
          <div className={style.formTitle}>
            {this.props.links.data.id ? 'Editar Enlace' : 'Crear Enlace'}
          </div>
          <form className={style.courseForm}>
            <Input {...{...config, id: 'title', label: 'Titulo'}} />
            <Input {...{...config, id: 'img', label: 'Imagen', type: 'file'}} />
            <Input {...{...config, id: 'text', label: 'Texto'}} />
            <Input {...{...config, id: 'href', label: 'URL'}} />
          </form>
          <div className={style.btns}>
            <button className={style.publishBtn} onClick={this.props.linksSave}>
              {this.props.links.process === 'save' ? <div><i className='fa fa-spinner fa-spin' /></div> : 'Guardar'}
            </button>
            {this.props.links.data.id &&
              <button className={style.publishBtn} onClick={e => this.props.setModal({modal: 'linkDelete', value: this.props.general.value})}>
                {this.props.links.process === 'delete' ? <div><i className='fa fa-spinner fa-spin' /></div> : 'Eliminar'}
              </button>
            }
            <a className={style.backBtn} onClick={this.props.closeAll}>Cerrar</a>
          </div>
        </div>
      </div>
    )
  }
}
