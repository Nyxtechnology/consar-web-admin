import React from 'react'

export default class Link extends React.Component {
  render () {
    return (
      <div
        key={this.props.item.id}
        onClick={() => this.props.setModal({modal: 'linkForm', value: this.props.item})}
        draggable
        onDragStart={this.props.onDragStart}
        onDragEnd={this.props.onDragEnd}
        onDragOver={this.props.onDragOver}
        onDragEnter={this.props.onDragEnter}
        onDragLeave={this.props.onDragLeave}
        onDrop={this.props.onDrop}
        style={{border: this.props.draggingOver.id === this.props.item.id ? '1px solid red' : ''}}>
        <div>
          <div>{this.props.item.title}</div>
        </div>
      </div>
    )
  }
}
