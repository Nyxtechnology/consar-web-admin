import React from 'react'
import style from './style.css'
import {Link} from 'react-router-dom'

class CategoriesList extends React.Component {
  componentDidMount () {
    if (!this.props.auth.user.token) return this.props.history.push('/')
    this.props.categoriesLoad()
  }
  render () {
    const process = this.props.categories.process
    return (
      <div className={style.listContainer}>
        <div className={style.listHeader}>
          <h3>Categorias</h3>
          <Link to='/categorias/nuevo'>Crear Categoria</Link>
        </div>
        <div className={style.list}>
          {this.props.categories.list.map(i => (
            <Link
              key={i.id}
              to={'/categorias/' + i.slug}>
              <div>{i.title}</div>
            </Link>
          ))}
        </div>
      </div>
    )
  }
}

export default CategoriesList
