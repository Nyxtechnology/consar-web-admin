import api from './api'
import FormData from './form'
import pick from 'lodash/fp/pick'

const aurl = process.env.api

export const sregister = (username, password) => api({
  url: aurl + 'api/register',
  method: 'post',
  body: FormData({username, password})
})

export const slogin = (username, password) => api({
  url: aurl + 'api/login',
  method: 'post',
  headers: {'Content-type': 'application/json'},
  body: JSON.stringify({username, password})
})

export const getCategories = (token, slug, params) => api({
  url: aurl + 'api/references/' + (slug || ''),
  method: 'get',
  params,
  headers: {
    'authorization': 'JWT ' + token
  }
})

export function saveCategory (token, data) {
  const err = {}
  if (!data.type) err.type = 'Debes seleccionar un tipo'
  if (!data.title) err.title = 'El nombre es requerido'
  if (!data.img0) err.img0 = 'Debes seleccionar una imagen'
  if (!data.img1) err.img1 = 'Debes seleccionar un imagen'
  if (!data.img2) err.img2 = 'Debes seleccionar un imagen'
  if (Object.values(err).length) return Promise.reject({status: 400, data: err})

  return api({
    url: aurl + 'api/references/' + (data.id ? data.id : ''),
    method: data.id ? 'put' : 'post',
    headers: {'authorization': 'JWT ' + token},
    body: FormData(pick(['type', 'title', 'img0', 'img1', 'img2'], data))
  })
}
export const deleteCategories = (token, id) => api({
  url: aurl + 'api/references/' + id,
  method: 'delete',
  headers: {
    'authorization': 'JWT ' + token
  }
})

export const getLinks = (token, slug, params) => api({
  url: aurl + 'api/referenceItems/' + (slug || ''),
  method: 'get',
  params,
  headers: {
    'authorization': 'JWT ' + token
  }
})

export function saveLinks (token, data) {
  const err = {}
  if (!data.referenceId) err.referenceId = 'Debes seleccionar una categía'
  if (!data.title) err.title = 'El nombre es requerido'
  if (!data.img) err.img = 'Debes seleccionar una imagen'
  if (!data.text) err.text = 'Debes ingresar un texto'
  if (!data.href) err.href = 'Debes ingresar un enlace'
  if (Object.values(err).length) return Promise.reject({status: 400, data: err})

  return api({
    url: aurl + 'api/referenceItems/' + (data.id ? data.id : ''),
    method: data.id ? 'put' : 'post',
    headers: {'authorization': 'JWT ' + token},
    body: FormData(pick(['referenceId', 'title', 'img', 'text', 'href'], data))
  })
}

export const deleteLinks = (token, id) => api({
  url: aurl + 'api/referenceItems/' + id,
  method: 'delete',
  headers: {
    'authorization': 'JWT ' + token
  }
})

export const reorderLinks = (token, data) => api({
  url: aurl + 'api/referenceitems/reorder',
  method: 'post',
  headers: {'authorization': 'JWT ' + token, 'Content-type': 'application/json'},
  body: JSON.stringify(data)
})

/** PROJECTS ****/

export const getProjects = (token, slug, params) => api({
  url: aurl + 'api/projects/' + (slug || ''),
  headers: {'authorization': 'JWT ' + token},
  method: 'get',
  params
})

export function saveProjects (token, data) {
  const err = {}
  if (!data.title) err.title = 'Este campo es requerido'
  if (!data.description) err.description = 'Este campo es requerido'
  if (!data.icon) err.icon = 'Este campo es requerido'
  if (!data.video) err.video = 'Este campo es requerido'

  if (Object.values(err).length) return Promise.reject({status: 400, data: err})

  return api({
    url: aurl + 'api/projects/' + (data.id ? data.id : ''),
    method: data.id ? 'put' : 'post',
    headers: {'authorization': 'JWT ' + token},
    body: FormData(pick(['title', 'description', 'text', 'icon', 'image', 'video'], data))
  })
}

export const deleteProjects = (token, id) => api({
  url: aurl + 'api/projects/' + id,
  headers: {'authorization': 'JWT ' + token},
  method: 'delete'
})

export const getUsers = (token, id, params) => api({
  url: aurl + 'api/users/' + (id || ''),
  headers: {'authorization': 'JWT ' + token},
  method: 'get',
  params
})

export function saveUsers (token, data) {
  const err = {}
  if (!data.username) err.username = 'Este campo es requerido'
  if (!data.password) err.password = 'Este campo es requerido'

  if (Object.values(err).length) return Promise.reject({status: 400, data: err})

  return api({
    url: aurl + 'api/users/' + (data.id ? data.id : ''),
    method: data.id ? 'put' : 'post',
    headers: {'authorization': 'JWT ' + token},
    body: FormData(pick(['username', 'password'], data))
  })
}

export const deleteUsers = (token, id) => api({
  url: aurl + 'api/users/' + id,
  headers: {'authorization': 'JWT ' + token},
  method: 'delete'
})
