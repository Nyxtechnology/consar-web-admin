import findIndex from 'lodash/findIndex'
const initialState = {
  process: null,
  err: null,
  step: 0,
  typeList: [{label: 'Publico', value: 'public'}, {label: 'Tema', value: 'theme'}],
  list: [],
  data: {
    type: 'public',
    title: '',
    img0: '',
    img1: '',
    img2: ''
  }
}

export default function categories (state = initialState, action) {
  switch (action.type) {
    case 'categoriesChange':
      return {...state, err: null, data: {...state.data, ...action.data}}
    case 'categoriesSelect':
      return {...state, err: null, data: state.data.id === action.data.id ? initialState.data : action.data}
    case 'categoriesInitReset':
      return initialState
    case 'categoriesInitAttempt':
      return {...state, process: true, err: null}
    case 'categoriesInitSuccess':
      return {...state, process: false, ...action.data}
    case 'categoriesReset':
      return {...state, data: initialState.data}
    case 'categoriesLoadAttempt':
      return {...state, process: 'load', err: null}
    case 'categoriesLoadSuccess':
      return {...state, process: false, err: null, list: action.data}
    case 'categoriesLoadError':
      return {...state, process: null, err: action.data}
    case 'categoriesLoadOneAttempt':
      return {...state, process: 'load', err: null}
    case 'categoriesLoadOneSuccess':
      return {...state, process: false, err: null, data: {...initialState.data, ...action.data}}
    case 'categoriesLoadOneError':
      return {...state, process: null, err: action.data}
    case 'categoriesSaveAttempt':
      return {...state, process: 'create', err: null}
    case 'categoriesSaveSuccess':
      return {
        ...initialState,
        data: action.data,
        list: findIndex(state.list, {id: action.data.id}) === -1
          ? [...state.list, action.data]
          : state.list.map(r => r.id === action.data.id ? action.data : r)
      }
    case 'categoriesSaveError':
      return {...state, process: null, err: action.data}
    case 'categoriesDeleteAttempt':
      return {...state, process: 'delete', err: null}
    case 'categoriesDeleteSuccess':
      return {
        ...initialState,
        list: state.list.filter(i => i.id !== action.data)
      }
    case 'categoriesDeleteError':
      return {...state, process: null, err: action.data}
    default:
      return state
  }
}
