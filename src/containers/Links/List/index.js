import React from 'react'
import Link from './Link'
import style from './style.css'

class LinksList extends React.Component {
  state = {dragging: false, draggingOver: false}
  componentDidMount () {
    this.props.linksLoad()
  }
  onDrop = e => {
    this.props.LinksReorder(this.state)
    this.setState({dragging: false, draggingOver: false})
  }
  render () {
    const process = this.props.links.process
    return (
      <div className={style.listContainer}>
        <div className={style.listHeader}>
          <h3>Enlaces</h3>
          <button onClick={() => this.props.setModal('linkForm')}>Crear Enlace</button>
        </div>
        <div className={style.list}>
          {this.props.links.list.map(item =>
            <Link
              key={item.id} {...{...this.props, item, ...this.state}}
              onDragStart={e => this.setState({dragging: item})}
              onDragEnd={e => this.setState({dragging: false, draggingOver: false})}
              onDragOver={e => e.preventDefault()}
              onDragEnter={e => this.setState({draggingOver: item})}
              onDragLeave={e => this.setState({draggingOver: false})}
              onDrop={e => this.onDrop(e)}
              />)}
        </div>
      </div>
    )
  }
}

export default LinksList
