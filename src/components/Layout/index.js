import React from 'react'
import style from './style.css'
import Header from '../Header/'

const Layout = props => (
  <div className={style.layoutContainer} onClick={props.general.modal ? props.closeAll : null}>
    <Header {...props} />
    <div className={style.contentWrapper}>{props.children}</div>
  </div>
)

export default Layout
