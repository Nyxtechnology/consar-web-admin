import * as Routes from './SyncRoutes'

const routes = [
  {path: '/', exact: true, component: Routes.Home},
  {path: '/login', exact: true, component: Routes.Login},
  {path: '/categorias', component: Routes.Categories},
  {path: '/links', component: Routes.Links},
  {path: '/proyectos', component: Routes.Projects},
  {path: '/usuarios', component: Routes.Users}
]

export default routes
