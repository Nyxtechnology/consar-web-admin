import React from 'react'
import {Input} from 'react-one-form'
import {Link} from 'react-router-dom'
import style from './style.css'

export default class ProjectsForm extends React.Component {
  componentDidMount () {
    if (this.props.match.params.slug) {
      this.props.projectsLoadOne(this.props.match.params.slug)
    } else {
      this.props.projectsReset()
    }
  }
  render () {
    const config = {store: this.props.projects, change: this.props.projectsInput, style}
    const process = this.props.projects.process
    return (
      <div className={style.formContainer}>
        <div className={style.formHeader}>
          <h3>{this.props.projects.data.id ? 'Editar Proyecto' : 'Crear Proyecto'}</h3>
          <Link className={style.backBtn} to='/proyectos'>Volver</Link>
        </div>
        <form className={style.courseForm}>
          <Input {...{...config, id: 'title', label: 'Titulo'}} />
          <Input {...{...config, id: 'description', label: 'Descripción'}} />
          <Input {...{...config, id: 'text', label: 'Texto'}} />
          <Input {...{...config, id: 'icon', label: 'Icono', type: 'file'}} />
          <Input {...{...config, id: 'video', label: 'Video', type: 'file'}} />
          <Input {...{...config, id: 'image', label: 'Imagen', type: 'file'}} />
        </form>
        <div className={style.btns}>
          <button className={style.publishBtn} onClick={this.props.projectsSave}>
            {process === 'save' ? <div><i className='fa fa-spinner fa-spin' /></div> : 'Guardar'}
          </button>
          {this.props.projects.data.id &&
            <button className={style.publishBtn} onClick={e => this.props.setModal('projectDelete')}>
              {process === 'delete' ? <div><i className='fa fa-spinner fa-spin' /></div> : 'Eliminar'}
            </button>
          }
        </div>
      </div>
    )
  }
}
